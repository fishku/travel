// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
// 解决部分安卓机型浏览器不支持 promise
import 'babel-polyfill'
// 轮播图插件
import VueAwesomeSwiper from 'vue-awesome-swiper'
// 样式重置文件
// styles目录是通过别名创建的,在webpack.base.config.js里
// import './assets/styles/reset.css'
import 'styles/reset.css'
// 解决移动端1像素边框的样式文件
// import './assets/styles/border.css'
import 'styles/border.css'
// 字体图标文件
// import './assets/styles/iconfont.css'
import 'styles/iconfont.css'
// 轮播图插件的样式
import 'swiper/dist/css/swiper.css'
// 解决移动端点击click事件300毫秒延迟问题
import fastClick from 'fastclick'

Vue.config.productionTip = false
// 注册轮播图插件
Vue.use(VueAwesomeSwiper)

// 配置使用fastClick
fastClick.attach(document.body)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
